package org.itstep.entity;

import javax.persistence.*;

@Entity
@Table(name = "file_data")
public class FileData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name ="file_id")
    Integer fileId;
    String description;
    byte [] file;

    public FileData() {
    }

    public FileData(Integer fileId, String description, byte[] file) {
        this.fileId = fileId;
        this.description = description;
        this.file = file;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }
}