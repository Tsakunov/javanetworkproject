package org.itstep.dto;

import java.util.Base64;

public class FileDTO {

    byte [] file;
    String description;

    public FileDTO() {
    }

    public FileDTO(byte[] file, String description) {
        this.file = file;
        this.description = description;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage()
    { if (file!=null) {
        return Base64.getEncoder().encodeToString(file);
    }
        return "";
    }
}
