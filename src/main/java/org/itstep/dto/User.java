package org.itstep.dto;

import java.util.Collection;
import java.util.stream.Collectors;

public class User {

    String user;
    String password;
    Collection<String> role;

    public User() {
    }

    public User(String user, String password, Collection<String> role) {
        this.user = user;
        this.password = password;
        this.role = role;
    }

    public User(String user) {
        this.user = user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(Collection<String> role) {
        this.role = role;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public Collection<String> getRole() {
        return role;
    }

    public String getRoleToString()
    {
        return getRole().stream().collect(Collectors.joining(","));
    }

    public static class MyRole{
        public static final String ADMIN="Admin";
        public static final String USER="User";
        public static final String BOSS="Boss";
    }
}