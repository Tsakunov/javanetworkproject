package org.itstep.сontrollers;

import org.itstep.dto.User;
import org.itstep.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller()
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    UserService userService;

//    @RequestMapping(value = "/", method = RequestMethod.POST)
//    public String hello(Model model) {
//
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        model.addAttribute("human", auth.getName());
//        model.addAttribute("roles", auth.getAuthorities().toString());
//        return "human";
//    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String userCreate() {
        return "user/userCreate";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String userCreate(@RequestParam(name = "login") String login, @RequestParam(name = "password") String password,
                         @RequestParam(name = "role") List<String> roles, Model model) {
        User user = new User(login, password, roles);
        User result = userService.create(user);
        model.addAttribute("isExists", result==null);
        model.addAttribute("user", user);
        return "user/userCreateResult";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteUserRoles() {
        return "user/userDelete";
    }

    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    public String deleteUserRoles(@RequestParam(name="login") String login,
                                  @RequestParam(name="role")List<String> roles,
                                  Model model){
        User user= new User();
        user.setUser(login);
        user.setRole(roles);
        List<String> result =userService.deleteRolesFromLogin(login, roles);
        model.addAttribute("deleted",result);
        model.addAttribute("login",login);
        return "user/userDeleteResult";
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String userUpdate() {
        return "user/userUpdate";
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public String userUpdate(@RequestParam(name = "login") String login,
                         @RequestParam(name = "password", required = false) String password,
                         @RequestParam(name = "role",required = false) List<String> roles, Model model)
    {
        User user = new User(login, password, roles);
        User result = userService.update(user);
        model.addAttribute(result);
        return "user/userUpdateResult";
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public String findUserByLogin() {
        return "user/userFind";
    }

    @RequestMapping(value = "/find",method = RequestMethod.POST)
    public String findUserByLogin(@RequestParam(name = "login") String login, Model model)
    {
        User user = new User(login);
        User result = userService.findUser(user);
        model.addAttribute(result);
        return "user/userFindResult";
    }
}
