package org.itstep.сontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FileRedirectController {
    @RequestMapping(value = "/fileAdd", method = RequestMethod.GET)
    public String fileAddRedirect(){
        return "files/filesAdd";
    }

    @RequestMapping(value = "/fileGet", method = RequestMethod.GET)
    public String fileGetRedirect(){
        return "files/filesGet";
    }
}
