package org.itstep.сontrollers;

import org.itstep.dto.FileDTO;
import org.itstep.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@Controller
@RequestMapping(value = "/files")
public class FileController {
    @Autowired
    FileService fileService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addFile() {
        return "files/filesAdd";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addFile(@RequestParam("file") MultipartFile file, @RequestParam(value = "description", required = false) String description) throws IOException,SQLException {
        FileDTO file1= new FileDTO();
        file1.setFile(file.getBytes());
        file1.setDescription(description);
        fileService.saveFile(file1);
        return "files/filesAddSuccess";
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public String getFile() {
        return "files/filesGet";
    }

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public String getFile(@RequestParam(value = "") String description) throws IOException,SQLException {
        FileDTO file= new FileDTO();
        file.setDescription(description);
        FileDTO findfile= fileService.loadFile(file);
        DataOutputStream os = new DataOutputStream(new FileOutputStream("d:\\binout.jpg"));
        os.write(findfile.getFile());
        os.close();
        return "files/filesGetSuccess";
    }

//    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
//    public String getAllFiles() {
//        return "files/filesGetAll";
//    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public String getAllFiles(Model model) throws IOException,SQLException {
        List<FileDTO> list =fileService.getAllFiles();
        model.addAttribute("list",list);
        return "files/filesGetAll";
    }
}
