package org.itstep.сontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserRedirectController {
    @RequestMapping(value = "/userFind", method = RequestMethod.GET)
    public String userFindRedirect(){
        return "user/userFind";
    }

    @RequestMapping(value = "/userCreate", method = RequestMethod.GET)
    public String userCreateRedirect(){
        return "user/userCreate";
    }

    @RequestMapping(value = "/userUpdate", method = RequestMethod.GET)
    public String userUpdateRedirect(){
        return "user/userUpdate";
    }

    @RequestMapping(value = "/userDelete", method = RequestMethod.GET)
    public String userDeleteRedirect(){
        return "user/userDelete";
    }
}
