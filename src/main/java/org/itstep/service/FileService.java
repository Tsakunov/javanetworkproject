package org.itstep.service;

import org.itstep.dto.FileDTO;
import org.itstep.entity.FileData;
import org.itstep.jpa.FileDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService {

    @Autowired
    FileDataRepository fileDataRepository;


    private FileData convert(FileDTO file)
    {
        FileData fileData = new FileData();
        fileData.setFile(file.getFile());
        fileData.setDescription(file.getDescription());
        return fileData;
    }

    public FileData saveFile(FileDTO file)throws SQLException {
        return fileDataRepository.save(convert(file));
    }

    public FileDTO loadFile(FileDTO file) {
        List<FileData> newFileData=fileDataRepository.findByDescription(file.getDescription());
        FileData tmp=newFileData.stream().findFirst().get();
        return new FileDTO(tmp.getFile(),tmp.getDescription());
    }

    public List<FileDTO> getAllFiles(){
        List<FileDTO> files= new ArrayList<>();
        List<FileData> fileData=fileDataRepository.findAll();
        for (FileData data:fileData) {
            FileDTO file= new FileDTO();
            file.setDescription(data.getDescription());
            file.setFile(data.getFile());
            files.add(file);
        }
        return files;
    }
}