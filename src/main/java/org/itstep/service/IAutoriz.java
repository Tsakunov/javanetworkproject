package org.itstep.service;

import org.itstep.dto.User;

import java.util.Collection;

public interface IAutoriz {
    void addUser(String login, String password, Collection<String> role);
    User getUserByLogin(String login);
    Collection<User> getAllUser();
}
