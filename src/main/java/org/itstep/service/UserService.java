package org.itstep.service;

import org.itstep.dto.User;
import org.itstep.entity.Login;
import org.itstep.entity.Role;
import org.itstep.jpa.LoginRepository;
import org.itstep.jpa.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    LoginRepository loginRepository;
    @Autowired
    RoleRepository roleRepository;

    public User create(User user)  {

        List<Login> logins= loginRepository.findByLogin(user.getUser());
        if(logins.isEmpty())
        {
            Login login = new Login();
            login.setLogin(user.getUser());
            login.setPassword(user.getPassword());
            List<Role> roleList = new ArrayList<>();
            user.getRole().forEach(r -> {
                roleList.add(new Role(login, r));
            });
            login.setRoles(roleList);
            Login result = loginRepository.save(login);
            return new User(result.getLogin(), result.getPassword(), result.getStringRoles());
        }
        return null;


    }
    @Transactional
    public List<String> deleteRolesFromLogin(String login, List<String> roles) {

        List<String> deletedRoles = new ArrayList<>();
        List<Login> logins=loginRepository.findByLogin(login);
        checkCountLogin(logins);
        for (Login mylogin:logins) {
            for (Role role:mylogin.getRoles()) {
                if(roles.contains(role.getName()))
                {
                    roleRepository.deleteRole(role.getId());

                    deletedRoles.add(role.getName());
                }
            }
        }
        return deletedRoles;
    }

    public User update(User user) {
        List<Login> logins = loginRepository.findByLogin(user.getUser());
        checkCountLogin(logins);
        for (Login login : logins) {
            if(user.getPassword()!=null&&!user.getPassword().isEmpty()) {
                login.setPassword(user.getPassword());
            }
            List<Role> roleList = new ArrayList<>();
            deleteRolesFromLogin(login.getLogin(), login.getStringRoles());
            if(user.getRole()!=null){
                user.getRole().forEach(r -> {
                    roleList.add(new Role(login, r));
                });
                login.setRoles(roleList);
            }
            Login result= loginRepository.save(login);
            return new User(result.getLogin(),result.getPassword(),result.getStringRoles());
        }
        return null;
    }

    private void checkCountLogin(List<Login> logins)
    {
        if(logins.isEmpty()){
            throw new RuntimeException("User not found");
        }
        if(logins.size()>1){
            throw new RuntimeException("User more than one");
        }
    }

    public User findUser(User user) {
        List<Login> logins= loginRepository.findByLogin(user.getUser());
        checkCountLogin( logins);
        Login login= logins.stream().findFirst().orElseThrow(()->new RuntimeException("login not found"));
        return new User(login.getLogin(),login.getPassword(),login.getStringRoles());
    }
}
