package org.itstep.service.storage;

import org.itstep.dto.User;
import org.itstep.service.IAutoriz;
import org.springframework.stereotype.Component;

import java.util.*;

@Component(value = "Class")
public class AutorizStorage implements IAutoriz {

    Map<String, User> autorizMap = new HashMap<>();

    public AutorizStorage() {
        addUser("user", "user", Collections.singletonList(User.MyRole.USER));
        addUser("admin", "admin", Arrays.asList(User.MyRole.USER, User.MyRole.ADMIN));
        addUser("boss", "boss", Arrays.asList(User.MyRole.USER, User.MyRole.ADMIN, User.MyRole.BOSS));
    }

    @Override
    public void addUser(String login, String password, Collection<String> role) {
        autorizMap.put(login, new User(login, password, role));
    }

    @Override
    public User getUserByLogin(String login) {
        return autorizMap.get(login);
    }

    @Override
    public Collection<User> getAllUser() {
        return autorizMap.values();
    }
}
