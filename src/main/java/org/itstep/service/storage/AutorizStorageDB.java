package org.itstep.service.storage;

import org.itstep.dto.User;
import org.itstep.jpa.LoginRepository;
import org.itstep.service.IAutoriz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component(value = "DB")
public class AutorizStorageDB implements IAutoriz {

    @Autowired
    LoginRepository loginRepository;

    @Override
    public void addUser(String login, String password, Collection<String> role) {

    }

    @Override
    public User getUserByLogin(String login) {
        return null;
    }

    @Override
    public Collection<User> getAllUser() {
        List<User> userList = new ArrayList<>();
        loginRepository.findAll().forEach(login -> {
            userList.add(new User(login.getLogin(), login.getPassword(), login.getStringRoles()));
        });
        return userList;
    }
}
