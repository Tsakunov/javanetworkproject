package org.itstep.jpa;

import org.itstep.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {
    @Modifying
    @Transactional
    @Query(value = "delete from role where id=:id ",nativeQuery = true)
    void deleteRole(@Param(value = "id")Integer id);

    @Query(value = "select name from role where login_id=:loginId",nativeQuery = true)
    List<String> findRoleNameByLoginId(@Param(value = "loginId") Integer loginId);
}
