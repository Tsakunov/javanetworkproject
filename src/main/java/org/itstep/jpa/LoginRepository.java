package org.itstep.jpa;

import org.itstep.entity.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface LoginRepository extends JpaRepository<Login,Integer> {
    List<Login> findByLogin(String login);
}
